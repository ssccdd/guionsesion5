# Ejecutando una tarea en un marco de ejecución pasado un tiempo establecido

En los ejemplos anteriores hemos utilizado del **marco Executor** la clase [`ThreadPoolExecutor`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ThreadPoolExecutor.html) que nos permite ejecutar tareas que han implementado la interfaz `Runnable` o `Callable` mediante la utilización de un **conjunto de hilos**, esto nos ha permitido no tener que preocuparnos de la gestión de los hilos para la ejecución de las tareas. Una vez que una tarea es enviada al **marco Executor** se ejecutará tan pronto sea posible, atendiendo a la configuración que tenga el **marco Executor**. Pero puede que se nos presenten problemas en los que no queremos que la ejecución de las tareas sea en el primer momento disponible. Para estos casos tenemos a nuestra disposición la clase [`ScheduledThreadPoolExecutor`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ScheduledThreadPoolExecutor.html).

En este ejemplo utilizaremos la clase `ScheduledThreadPoolExecutor` para planificar la ejecución de las tareas después de un periodo de tiempo establecido.

1. Como en los ejemplos anteriores, definimos una clase para la tarea llamada `Task` que implementa la interface `Callable` *parametrizada* mediante la clase `String` como tipo de dato calculado. También podríamos haber definido la tarea implementando la interface `Runnable`.

2. Definimos una variable de instancia donde se almacenará el nombre de la tarea. De esta forma la podremos distinguir del resto y así comprobar si se ha realizado la planificación de forma correcta.

3. Implementamos el método `call()` para simular la tarea a realizar. En este ejemplo sólo se mostrará la fecha en que se ejecuta la tarea y devolverá la cadena *Hello World*.

```java
...
/**
 * Main method of the task. Writes a message to the console with
 * the actual date and returns the 'Hello world' string
 */
@Override
public String call() throws Exception {
    System.out.printf("%s: Starting at : %s\n",name,new Date());
    return "Hello, world";
}
...
```

4. Para poder realizar la demostración de la planificación del **marco Executor** implementamos el método `main()` de la aplicación. El **marco Executor** ahora estará representado por un objeto de la clase `ScheduledThreadPoolExecutor`. Lo crearemos con un sólo hilo para **conjunto de hilos**.

```java
...
// Create a ScheduledThreadPoolExecutor
ScheduledExecutorService executor=
            (ScheduledExecutorService)Executors.newScheduledThreadPool(1);
...
```

5. Ahora creamos `5` tareas que iremos que pasaremos al **marco Executor** con una planificación creciente, entre ellas habrá un intervalo de `1` segundo, y para ello utilizamos el método `schedule(.)` del objeto `Executor`.

```java
...
System.out.printf("Main: Starting at: %s\n",new Date());
		
// Send the tasks to the executor with the specified delay
for (int i=0; i<5; i++) {
    Task task=new Task("Task "+i);
    executor.schedule(task,i+1 , TimeUnit.SECONDS);
}
...
```

6. Lo siguiente es finalizar el **marco Executor** para esperar a que todas las tareas finalicen y así finalizar la aplicación principal.

```java
...
// Finish the executor
executor.shutdown();
		
// Waits for the finalization of the executor
try {
    executor.awaitTermination(1, TimeUnit.DAYS);
} catch (InterruptedException e) {
    e.printStackTrace();
}

// Writes the finalization message
System.out.printf("Core: Ends at: %s\n",new Date());
...
```

## Información adicional

En el ejemplo no hemos utilizado el valor calculado por las tareas y las diferentes tareas presentarán un mensaje por la salida estándar con un intervalo de `1` segundo y están ordenadas. ¿Podrías explicar este hecho?
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTI1NDgxNDA2N119
-->