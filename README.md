[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# GUIÓN DE PRÁCTICAS[^nota1]
## Sesión 5: Marco Executor (II)

Desde Java 5 tenemos a nuestra disposición un mecanismo que nos permite evitar estos problemas. El mecanismo en cuestión se llama el **marco Executor** y se encuentra en la interface [`Executor`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Executor.html), y sus subinterfaces [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html), y la clase [`ThreadPoolExecutor`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ThreadPoolExecutor.html) que implementa ambas interfaces.

Este guión en continuación del anterior donde se seguirán presentando características del **marco Executor** para la programación de aplicaciones concurrentes.

1. [Ejecutando una tarea en un **marco de ejecución** pasado un tiempo establecido.](https://gitlab.com/ssccdd/guionsesion5/-/blob/master/Ejercicio1.md)
2. [Ejecutando una tarea en un **marco de ejecución** periódicamente.](https://gitlab.com/ssccdd/guionsesion5/-/blob/master/Ejercicio2.md)
3. [Cancelando una tarea en un **marco de ejecución**.](https://gitlab.com/ssccdd/guionsesion5/-/blob/master/Ejercicio3.md)
4. [Controlando la finalización de una tarea en un **marco de ejecución**.](https://gitlab.com/ssccdd/guionsesion5/-/blob/master/Ejercicio4.md)
5. [Separando la ejecución de las tareas y el procesado de sus resultados en un **marco de ejecución**.](https://gitlab.com/ssccdd/guionsesion5/-/blob/master/Ejercicio5.md)
6. [Controlando el rechazo de tareas en un **marco de ejecución**.](https://gitlab.com/ssccdd/guionsesion5/-/blob/master/Ejercicio6.md)

---
[^nota1]: El guión se extrae del libro *Java 7 Concurrency Cookbook* que se encuentra disponible para los alumnos de la [Universidad de Jaén](https://www.ujaen.es/) por medio de su servicio de [Biblioteca Digital](http://www.ujaen.debiblio.com/login?url=https://learning.oreilly.com/home/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTM1NjUzNTgzNSwtMzk5MTk3MTk3XX0=
-->