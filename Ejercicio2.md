# Ejecutando una tarea en un marco de ejecución periódicamente

Si utilizamos la clase [`ThreadPoolExecutor`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ThreadPoolExecutor.html) como herramienta del **marco Executor** para la ejecución de las tareas concurrentes, una vez que dicha tarea se ha completado será eliminada del **marco Executor**. Si queremos que la tarea vuelva a ser ejecutada, tenemos que volver a añadirla en el **marco Executor**. Pero si utilizamos la clase [`ScheduledThreadPoolExecutor`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ScheduledThreadPoolExecutor.html) tenemos la posibilidad de ejecutar periódicamente la tarea.

En este ejemplo veremos cómo debemos utilizar esta capacidad del **marco de ejecución**.

1. Definimos la tarea mediante la clase `Task` que implementa la interface `Runnable`. Tendrá una variable de instancia para ponerle nombre a la tarea.

2. Implementamos el método `run()` para simular lo que la tarea deberá realizar. En el ejemplo sólo presentamos por la salida estándar un mensaje que indica el nombre de la tarea y la fecha de ejecución.

```java
...
/**
 * Main method of the example. Writes a message to the console with the actual
 * date
 */
@Override
public void run() {
    System.out.printf("%s: Executed at: %s\n",name,new Date());
}
...
```

3. Implementamos el método `main(.)` de la aplicación para probar la ejecución periódica de la tarea que hemos creado anteriormente. Creamos un objeto `Executor` de la clase `ScheduledTheadPoolExecutor` con un único hilo para el **conjunto de hilos**.

```java
...
// Create a ScheduledThreadPoolExecutor
ScheduledExecutorService executor=Executors.newScheduledThreadPool(1);
System.out.printf("Main: Starting at: %s\n",new Date());
...
```

4. Ahora creamos la tarea y la enviamos al objeto `Executor`. En este ejemplo utilizamos el método `scheduledAtFixRate(.)` que nos permitirá indicar cuándo queremos que se inicie la tarea y la periodicidad que queremos, además indicaremos la unidad de tiempo que utilizamos. El valor que nos devuelve será un objeto [`ScheduledFuture`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ScheduledFuture.html).

5. Para demostrar el ejemplo repetiremos `10` veces una serie de acciones. La primera será presentar por la salida estándar el tiempo que queda para la siguiente ejecución de la tarea. Para ello utilizaremos el objeto devuelto por la planificación del `Executor`. El método será `getDelay(.)` al que deberemos pasar la unidad de tiempo en la que queremos mostrar el resultado. Esperaremos por un tiempo antes de volver a repetir la operación.

```java
...
// Controlling the execution of tasks
for (int i=0; i<10; i++){
    System.out.printf("Main: Delay: %d\n",result.getDelay(TimeUnit.MILLISECONDS));
    try {
        TimeUnit.MILLISECONDS.sleep(500);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
}
...
```

6. Para terminar, finalizamos el **marco Executor** y damos por concluido el ejemplo.

## Ejercicio propuesto

Revisar el libro *Java 7 Concurrency Cookbook*,  se encuentra disponible para los alumnos de la [Universidad de Jaén](https://www.ujaen.es/) por medio de su servicio de [Biblioteca Digital](http://www.ujaen.debiblio.com/login?url=https://learning.oreilly.com/home/), para comprobar la existencia de otros métodos a nuestra disposición para realizar una planificación similar a la presentada en este ejemplo.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTQ4MjE3MTE2NV19
-->