# Controlando la finalización de una tarea en un marco de ejecución

La clase [`FutureTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/FutureTask.html) proporciona un método llamado `done()` que nos permite ejecutar código después de la finalización de una tarea ejecutada en un **marco Executor**. De esta forma se pueden realizar diversas operaciones de *post-procesamiento*, generar un informe, enviar un e-mail con los resultados, o liberar ciertos recursos. El método es invocado internamente por la clase `FutureTask` cuando la ejecución de la tarea, que el objeto `FutureTask` está controlando, finaliza. El método es invocado después que el resultado de la tarea es devuelto y su estado cambia a `isDone`, también lo será cuando la tarea sea cancelada.

Por defecto, el método está vacío. Debemos implementar el método para cambiar este comportamiento. En el ejemplo vemos cómo se implementa el método para ejecutar un código después de la ejecución de la tarea.

1. Primero definimos una clase llamada `ExecutableTask` que implementa la interface `Callable` *parametrizada* por la clase `String` como elemento calculado. Tenemos una variable de instancia para almacenar el nombre de la tarea. Debemos proporcionar un método para obtener dicho nombre.

2. Implementamos el método `call()` para simular las operaciones que realizará la tarea. En nuestro ejemplo, presentamos por la salida estándar el tiempo que llevará realizar la tarea. Pasado ese tiempo se devuelve una cadena que identifica la finalización de la misma con el nombre de la tarea.

```java
...
/**
 * Main method of the task. It waits a random period of time and returns a message
 */
@Override
public String call() throws Exception {
    try {
        Long duration=(long)(Math.random()*10);
        System.out.printf("%s: Waiting %d seconds for results.\n",this.name,duration);
        TimeUnit.SECONDS.sleep(duration);
    } catch (InterruptedException e) {
    }
    return "Hello, world. I'm "+name;
}
...
```

3. Ahora definimos la clase llamada `ResultTask` que hereda de la clase `FutureTask` *parametrizada* por la clase `String` al ser el elemento calculado por la tarea. Creamos una variable de instancia para almacenar el nombre de la tarea asociada.

4. Definimos el constructor para la clase. Tendrá como parámetro un objeto que implementa la interface `Callable`. Se invocará al constructor de la clase padre y se inicializa la variable de instancia que almacena el nombre de la tarea asociada.

```java
...
/**
 * Constructor of the Class. Override one of the constructor of its parent class 
 * @param callable The task this object manages
 */
public ResultTask(Callable<String> callable) {
    super(callable);
    this.name=((ExecutableTask)callable).getName();
}
...
```

5. Sólo queda implementar el método `done()`. Comprobamos si se ha invocado por la cancelación de la tarea. De esta forma se presentarán dos mensajes diferentes, atendiendo a si la tarea se completó o fue cancelada.

```java
...
/**
 * Method that is called when the task finish.
 */
@Override
protected void done() {
    if (isCancelled()) {
        System.out.printf("%s: Has been cancelled\n",name);
    } else {
        System.out.printf("%s: Has finished\n",name);
    }
}
...
```

6. Para finalizar, implementamos el método `main(.)` de la aplicación. Lo primero es crear el **marco Executor**.

7. Creamos un array para almacenar `5` objetos de la clase `ResultTask`. Ahora deberemos ir completando cada uno de los elementos del array. Para ello, creamos un objeto de la clase `ExecutableTask` al que le asociamos un nombre. A continuación, el elemento del array se le asocia un objeto de la clase `ResultTask` con la tarea anteriormente creada. Por último, se envía al **marco Executor**.

```java
...
//Create five tasks
ResultTask resultTasks[]=new ResultTask[5];
for (int i=0; i<5; i++) {
    ExecutableTask executableTask=new ExecutableTask("Task "+i);
    resultTasks[i]=new ResultTask(executableTask);
    executor.submit(resultTasks[i]);
}
...
```

8. Interrumpimos la ejecución del hilo principal por `5` segundos.

9. Pasados esos `5` segundos, cancelamos la ejecución de las `5` tareas. Aquellas tareas que ya han finalizado ignoran la cancelación.

```java
...
// Cancel all the tasks. In the tasks that have finished before this moment, this
// cancellation has no effects
for (int i=0; i<resultTasks.length; i++) {
    resultTasks[i].cancel(true);
}
...
```

10. Para terminar, presentamos por pantalla el resultado de las tareas finalizaron correctamente. Para ello debemos invocar el método `get()` de la clase `FutureTask`. Nos devolverá el objeto calculado por la tarea asociada a cada uno de los objetos `ResultTask`.

```java
...
// Write the results of those tasks that haven't been cancelled
for (int i=0; i<resultTasks.length; i++) {
    try {
        if (!resultTasks[i].isCancelled()){
            System.out.printf("%s\n",resultTasks[i].get());
        }
    } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
    } 
}
...
```

11. No debemos olvidar terminar con el **marco Executor**.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEzMTk5MDQ4NjZdfQ==
-->