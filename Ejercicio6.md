# Controlando el rechazo de tareas en un marco de ejecución

Cuando se termina con la ejecución de un **marco Executor**, hemos utilizado el método `shutdown()`, para no añadir nuevas tareas al **marco Executor** y esperar a la finalización de las tareas que aún quedan pendientes. Si se envía una nueva tarea a un **marco Executor**, una vez que se ha solicitado su finalización, la tarea será rechazada. La clase [`ThreadPoolExecutor`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ThreadPoolExecutor.html) proporciona un mecanismo, que será invocado cuando la tarea es rechazada.

En el ejemplo se presenta como se puede tratar las tareas que son rechazadas para su ejecución en un **marco Executor**. Para ello utilizaremos la interfaz [`RejectedExecutionHandler`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RejectedExecutionHandler.html).

1. Definimos una clase llamada `RejectedTaskController` que implementa la interface `RejectedExecutionHandler`. Para ello debemos implementar el método `rejectedExecution(.)`. Este método nos permite establecer el comportamiento deseado cuando una tarea es rechazada su ejecución en el **marco Executor**.

```java
...
/**
 * Method that will be executed for each rejected task
 * @param r Task that has been rejected
 * @param executor Executor that has rejected the task
 */
@Override
public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
    System.out.printf("RejectedTaskController: The task %s has been rejected\n",r.toString());
    System.out.printf("RejectedTaskController: %s\n",executor.toString());
    System.out.printf("RejectedTaskController: Terminating: %s\n",executor.isTerminating());
    System.out.printf("RejectedTaksController: Terminated: %s\n",executor.isTerminated());
}
...
```

2. Definimos una clase llamada `Task` que implementa la interface `Runnable` para simular una tarea que deberá ejecutarse en el **marco Executor**. Creamos una variable de instancia para almacenar el nombre de la tarea.

3. El método `run()` simulará la ejecución de la tarea. Como en otros casos presentamos un mensaje por la salida estándar para indicar el tiempo que llevará la tarea y se esperará un tiempo aleatorio.

```java
...
/**
 * Main method of the task. Waits a random period of time
 */
@Override
public void run() {
    System.out.printf("Task %s: Starting\n",name);
    try {
        Long duration=(long)(Math.random()*10);
        System.out.printf("Task %s: ReportGenerator: Generating a report during %d seconds\n",name,duration);
        TimeUnit.SECONDS.sleep(duration);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    System.out.printf("Task %s: Ending\n",name);
}
...
```

4. Implementamos el método `main(.)` de la aplicación. Creamos un objeto para tener el **marco Executor**. También creamos un objeto de la clase `RejectedTaskController`. Para establecer el comportamiento deseado para el **marco Executor** utilizamos el método `setRejectedExecutionHandler(.)`.

```java
...
// Create the controller for the Rejected tasks
RejectedTaskController controller=new RejectedTaskController();
// Create the executor and establish the controller for the Rejected tasks
ThreadPoolExecutor executor=(ThreadPoolExecutor)Executors.newCachedThreadPool();
executor.setRejectedExecutionHandler(controller);
...
```

5. Ahora crearemos una serie de tareas, objetos de la clase `Task`, para enviarlos al **marco Executor**.

```java
...
// Lauch three tasks
System.out.printf("Main: Starting.\n");
for (int i=0; i<3; i++) {
    Task task=new Task("Task"+i);
    executor.submit(task);
}
...
```

6. Terminamos con la ejecución del **marco Executor** mediante el método `shutdown()`.

7. Para comprobar el comportamiento que hemos programado para el rechazo de las tareas al **marco Executor** volveremos a enviar una nueva tarea. Y finalizamos con la aplicación.

```java
...
// Shutdown the executor
System.out.printf("Main: Shuting down the Executor.\n");
executor.shutdown();

// Send another task
System.out.printf("Main: Sending another Task.\n");
Task task=new Task("RejectedTask");
executor.submit(task);
		
// The program ends
System.out.printf("Main: End.\n");
...
```


<!--stackedit_data:
eyJoaXN0b3J5IjpbMTE3NTAzNzI3XX0=
-->