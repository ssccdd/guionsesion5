# Cancelando una tarea en un marco de ejecución

Cuando utilizamos un **marco Executor** no tenemos que tratar con los hilos necesarios para la ejecución de nuestras tareas. Sólo debemos preocuparnos de implementar nuestras clases con las interfaces `Runnable` o `Callable` y enviarlas el objeto [`Executor`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Executor.html) para que puedan ejecutarse. Es el objeto `Executor` el que se encarga de gestionar los hilos que necesita para la ejecución de las tareas. Es muy posible que deseemos cancelar una tarea, si ya no es útil, una vez que ya ha sido enviada al **marco Executor**. Para ello utilizamos el método `cancel(.)` de la interface [`Future`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Future.html) que nos permite establecer la operación de cancelación.

En el ejemplo mostramos los pasos necesarios para poder enviar la solicitud de cancelación a una tarea y comprobar que ha sido cancelada.

1. Definimos la tarea mediante la clase `Task` que implementa la interface `Callable` *parametrizada* por la clase `String` como valor calculado.

2. Sólo implementamos el método `call()` para simular una tarea continua que presenta por la salida estándar un mensaje. Entre un mensaje y otro la tarea se suspenderá por un tiempo.

```java
...
/**
 * Main method of the task. It has an infinite loop that writes a message to
 * the console every 100 milliseconds
 */
@Override
public String call() throws Exception {
    while (true){
        System.out.printf("Task: Test\n");
        Thread.sleep(100);
    }
}
...
```

3. Ahora debemos implementar el método `main(.)` de la aplicación para demostrar cómo debemos cancelar la tarea. Primero creamos el objeto `Ejecutor`, creamos la tarea y la enviamos al **marco de ejecución**. Antes de solicitar la cancelación de la tarea, suspendemos la ejecución del hilo principal por `2` segundos.

4. Pasado ese tiempo, se invoca el método `cancel(.)` para cancelar la tarea. Se comprueba si se ha cancelado efectivamente y se finaliza el **marco Executor**.

```java
...
// Cancel the task, finishing its execution
System.out.printf("Main: Cancelling the Task\n");
result.cancel(true);
// Verify that the task has been cancelled
System.out.printf("Main: Cancelled: %s\n",result.isCancelled());
System.out.printf("Main: Done: %s\n",result.isDone());
		
// Shutdown the executor
executor.shutdown();
System.out.printf("Main: The executor has finished\n");
...
```


<!--stackedit_data:
eyJoaXN0b3J5IjpbOTc3NzUzMjkwXX0=
-->