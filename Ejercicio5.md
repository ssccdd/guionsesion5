# Separando la ejecución de las tareas y el procesado de sus resultados en un marco de ejecución

Hasta el momento, cuando enviamos una tarea al **marco Executor** para obtener sus resultados y estado, utilizamos un objeto que implementa la interface [`Future`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Future.html). Pero podemos tener situaciones, donde necesitamos enviar la tarea al **marco Executor** en un objeto pero procesar el resultado mediante otro objeto. Para estos casos Java nos proporciona la interface [`CompletionService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CompletionService.html).

La interface `CompletionService` tiene un método para enviar la tarea a un **marco Executor** y un método para obtener un objeto `Future` para la siguiente tarea que ha finalizado su ejecución. Internamente, está utilizando un **marco Executor** para ejecutar las tareas. Este comportamiento es la ventaja para compartir objetos `CompletionService`, y enviar las tareas a un **marco Executor** y otro puede procesar los resultados. La limitación es que este segundo objeto sólo puede obtener el objeto `Future` para aquellas tareas que han finalizado su ejecución, por tanto, esos objetos `Future` sólo pueden ser utilizados para obtener los resultados de las tareas.

En el ejemplo aprenderemos como utilizar la interface `CompletionService` para separar la ejecución de la tarea en un **marco Executor** del procesamiento de sus resultados.

1. Definimos una clase llamada `ReportGenerator` que implementa la interface `Callable` *parametrizada* por la clase `String` como elemento calculado. Creamos dos variables de instancia para almacenar el nombre del emisor y el título del informe que generará.

2. Implementamos el método `call()` para simular la generación del informe. Para ello simulamos un tiempo de procesamiento y presenta la información en la salida estándar. Para finalizar devuelve el resultado de la tarea.

```java
...
/**
 * Main method of the ReportGenerator. Waits a random period of time
 * and then generates the report as a String.
 */
@Override
public String call() throws Exception {
    try {
        Long duration=(long)(Math.random()*10);
        System.out.printf("%s_%s: ReportGenerator: Generating a report during %d seconds\n",this.sender,this.title,duration);
        TimeUnit.SECONDS.sleep(duration);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    String ret=sender+": "+title;
    return ret;
}
...
```

3. Definimos una clase llamada `ReportRequest` que implementa la interface `Runnable` que pretende simular la clase que realiza la solicitud para realizar el informe.

4. Definimos dos variables de instancia, una que permitirá almacenar el nombre que será el que solicita el informe. Y una segunda para un objeto que implemente la interface `CompletionService` donde se enviará la tarea que simula la realización del informe, `ReportGenerator`, para que se ejecute. El constructor establecerá los valores apropiados para estas dos variables de instancia.

5. Implementamos el método `run()` que realiza la creación de la tarea para generar el informe y se enviará para su ejecución.

```java
...
/**
 * Main method of the class. Create three ReportGenerator tasks and executes them
 * through a CompletionService
 */
@Override
public void run() {
    ReportGenerator reportGenerator=new ReportGenerator(name, "Report");
    service.submit(reportGenerator);
}
...
```

6. Definimos una clase llamada `ReportProcessor` que implementa la interface `Runnable`. Esta clase simulará el tratamiento del resultado de la tarea `ReportGenerator`.

7. Como en la clase anterior, necesitamos dos variables de instancia. Una primera para un objeto que implementa la interface `CompletionService` desde donde se recogerán los resultados de las tareas que hayan finalizado. Además una variable que indique si se ha finalizado con el procesado de los informes.

8. Implementamos el método `run()` para simular el procesamiento de los informes. Comprobamos si se ha finalizado al generación de los informes como primer paso.  Obtenemos el objeto `Future` de la tarea que haya finalizado pasados `20` segundos mediante el método `poll(.)`. Si en ese momento no ha finalizado volvemos a realizar otra pasada. Si hay una que haya finalizado se presenta el resultado.

```java
...
/**
 * Main method of the class. While the variable end is false, it
 * calls the poll method of the CompletionService and waits 20 seconds
 * for the end of a ReportGenerator task
 */
@Override
public void run() {
    while (!end){
        try {
            Future<String> result=service.poll(20, TimeUnit.SECONDS);
            if (result!=null) {
                String report=result.get();
                System.out.printf("ReportReceiver: Report Recived: %s\n",report);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
    
    System.out.printf("ReportSender: End\n");
}
...
```

9. Necesitamos un método adicional llamado `setEnd(.)` para indicar que se ha finalizado el procesamiento de los informes.

10. Implementamos el método `main(.)` de la aplicación. Debemos crear un objeto para tener un **marco Executor**. Este objeto será necesario para el objeto que implemente la interface `CompletionService`, como se ha indicado al inicio del ejemplo, para poder ejecutar las tareas que realizan la petición del informe.

```java
...
// Create the executor and thee CompletionService using that executor
ExecutorService executor=(ExecutorService)Executors.newCachedThreadPool();
CompletionService<String> service=new ExecutorCompletionService<>(executor);
...
```

11. Se crearán dos objetos de la clase `ReportRequest` que será el encargado enviar las tareas para realizar los informes al `CompletionService`. Además crearemos los objetos de la clase `Thread` para que se ejecuten.

```java
...
// Crete two ReportRequest objects and two Threads to execute them
ReportRequest faceRequest=new ReportRequest("Face", service);
ReportRequest onlineRequest=new ReportRequest("Online", service);
Thread faceThread=new Thread(faceRequest);
Thread onlineThread=new Thread(onlineRequest);
...
```

12. También creamos un objeto de la clase `ReportProcessor` que será el encargado de procesar el resultado de las tareas que realizan los informes. También se creará un objeto de la clase `Thread` para ejecutar esta tarea.

```java
...
// Create a ReportSender object and a Thread to execute  it
ReportProcessor processor=new ReportProcessor(service);
Thread senderThread=new Thread(processor);
...
```

13. Para finalizar, una vez que se lanzan los objetos de la clase `Thread`, el hilo principal esperará hasta que finalicen las tareas que han enviado las solicitudes para la realización de los informes.

14. Se terminará con la ejecución del **marco Executor** y se esperará a la finalización del procesamiento de los informes.

```java
...
// Wait for the end of the ReportGenerator tasks
try {
    System.out.printf("Main: Waiting for the report generators.\n");
    faceThread.join();
    onlineThread.join();
} catch (InterruptedException e) {
    e.printStackTrace();
}
		
// Shutdown the executor
System.out.printf("Main: Shuting down the executor.\n");
executor.shutdown();
try {
    executor.awaitTermination(1, TimeUnit.DAYS);
} catch (InterruptedException e) {
    e.printStackTrace();
}
// End the execution of the ReportSender
processor.setEnd(true);
System.out.printf("Main: Ends\n");
...
```

## Ejercicio Propuesto

Modificar el ejemplo anterior para que en el programa principal no sea necesaria la utilización de objetos de la clase `Thread` para ejecutar las tareas implementadas por las clases `ReportRequest` y `ReportProcessor`.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTczMDQyMDk0Ml19
-->